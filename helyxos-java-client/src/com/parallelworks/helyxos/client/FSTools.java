/*
 * Created on Aug 31, 2016
 */
package com.parallelworks.helyxos.client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

public class FSTools {
    
    private static class ExtendedFileVisitor<T> extends SimpleFileVisitor<T> {
        public void close() throws IOException {
        }
    }
    
    public static ExtendedFileVisitor<Path> print() {
        return new ExtendedFileVisitor<Path>() {
            @Override
            public void close() throws IOException {
                System.out.println("Done");
            }

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                System.out.println(attrsToString(attrs) + "\t" + dir);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println(attrsToString(attrs) + "\t" + file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                System.out.println(file + "\t" + exc.getMessage());
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }
        };
    }
    
    protected static String attrsToString(BasicFileAttributes attrs) {
        StringBuilder sb = new StringBuilder();
        append(sb, attrs.isDirectory(), "d", "-");
        sb.append(" ");
        sb.append(String.format("%20d ", attrs.size()));
        return sb.toString();
    }

    private static void append(StringBuilder sb, boolean pred, String st, String sf) {
        if (pred) {
            sb.append(st);
        }
        else {
            sb.append(sf);
        }
    }

    public static ExtendedFileVisitor<Path> deleteFile() {
        return new ExtendedFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                deleteFile(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                deleteFile(dir);
                return FileVisitResult.CONTINUE;
            }
        };
    }
    
    private static final Path CWD = Paths.get(".").toAbsolutePath().normalize();
    
    public static void deleteFile(Path p) {
        if (!p.toAbsolutePath().startsWith(CWD)) {
            System.out.println("CWD: " + CWD);
            System.out.println("  F: " + p.toAbsolutePath());
            throw new IllegalArgumentException("Cowardly refusing to delete file outside of CWD (" + p + ")");
        }
        p.toFile().delete();
    }
    
    public static ExtendedFileVisitor<Path> targz(String outFileName) throws IOException {
        OutputStream os = new BufferedOutputStream(new FileOutputStream(outFileName));
        GzipCompressorOutputStream gzs = new GzipCompressorOutputStream(os);
        TarArchiveOutputStream ts = new TarArchiveOutputStream(gzs);
        
        return archive(ts);
    }
    
    private static ExtendedFileVisitor<Path> archive(final ArchiveOutputStream ts) {
        
        return new ExtendedFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                ArchiveEntry entry = new TarArchiveEntry(dir.toFile());
                ts.putArchiveEntry(entry);
                ts.closeArchiveEntry();
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                File f = file.toFile();
                ArchiveEntry entry = new TarArchiveEntry(f);
                ts.putArchiveEntry(entry);
                FileInputStream fis = new FileInputStream(f);
                try {
                    IOUtils.copy(fis, ts);
                    ts.closeArchiveEntry();
                }
                finally {
                    fis.close();
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                System.err.println(file.toString() + ": " + exc.getMessage());
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public void close() throws IOException {
                ts.close();
            }
        };
    }
    
    private static void addFile(ArchiveOutputStream aos, File file) throws IOException {
        ArchiveEntry entry = new TarArchiveEntry(file);
        aos.putArchiveEntry(entry);

        try {
            if (file.isFile()) {
                FileInputStream fis = new FileInputStream(file);
                try {
                    IOUtils.copy(fis, aos);
                }
                finally {
                    fis.close();
                }
            }
        }
        finally {
            aos.closeArchiveEntry();
        }
    }
    
    private static final PathMatcher ALL_FILES = FileSystems.getDefault().getPathMatcher("glob:*");

    public static PathMatcher files(String... globs) {
        if (globs == null || globs.length == 0) {
            return ALL_FILES;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("glob:{");
        for (int i = 0; i < globs.length; i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(globs[i]);
        }
        sb.append("}");
        return FileSystems.getDefault().getPathMatcher(sb.toString());
    }
    
    public static PathMatcher exclude(String... globs) {
        if (globs == null || globs.length == 0) {
            return null;
        }
        return files(globs);
    }
    
    public static void fileFinder(PathMatcher files, ExtendedFileVisitor<Path> v) throws IOException {
        fileFinder(".", files, null, v);
    }
    
    public static void fileFinder(PathMatcher files, PathMatcher exclude, ExtendedFileVisitor<Path> v) throws IOException {
        fileFinder(".", files, exclude, v);
    }
    
    public static void fileFinder(String location, PathMatcher files, ExtendedFileVisitor<Path> v) throws IOException {
        fileFinder(location, files, null, v);
    }
    
    /*
     * The general contract of command <files> is that path name expansion happens on <files> first, 
     * then the command recurses through directories and the exclusion crieteria is applied.
     *
     * This is relevant because one can say rm d1* d2/file. If this was done by first recursing and then
     * checking against the matching patterns, d2/ may not really match any of the patterns. So the 
     * many file arguments act not as much as an inclusion filter, but as root elements for independent
     * recursive removal operations.
     */
    
    private static class TopLevelFileVisitor extends SimpleFileVisitor<Path> {
        private final Path loc;
        private final PathMatcher files, exclude;
        private final ExtendedFileVisitor<Path> v;
        
        public TopLevelFileVisitor(Path loc, PathMatcher files, PathMatcher exclude, ExtendedFileVisitor<Path> v) {
            this.loc = loc;
            this.files = files;
            this.exclude = exclude;
            this.v = v;
        }
        
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            dir = loc.relativize(dir);
            if (dir.toString().equals("")) {
                // skip "."
                return FileVisitResult.CONTINUE;
            }
            if (matches(dir)) {
                v.preVisitDirectory(dir, attrs);
                Path target = loc.resolve(dir);
                Files.walkFileTree(target, new DeepFileVisitor(loc, dir, exclude, v));
                return FileVisitResult.SKIP_SUBTREE;
            }
            else {
                return FileVisitResult.CONTINUE;
            }
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            file = loc.relativize(file);
            if (matches(file)) {
                return v.visitFile(file, attrs);
            }
            else {
                return FileVisitResult.CONTINUE;
            }
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            return v.visitFileFailed(file, exc);
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            if (matches(dir)) {
                return v.postVisitDirectory(dir, exc);
            }
            else {
                return FileVisitResult.CONTINUE;
            }
        }
        
        private boolean matches(Path path) {
            return files.matches(path) && (exclude == null || !exclude.matches(path));
        }
    }
    
    private static class DeepFileVisitor extends SimpleFileVisitor<Path> {
        private final Path loc, root;
        private final PathMatcher exclude;
        private final ExtendedFileVisitor<Path> v;
        
        public DeepFileVisitor(Path root, Path loc, PathMatcher exclude, ExtendedFileVisitor<Path> v) {
            this.root = root;
            this.loc = loc;
            this.exclude = exclude;
            this.v = v;
        }
        
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            dir = root.relativize(dir);
            if (dir.equals(loc)) {
                // skip because it's already handled by the top level
                return FileVisitResult.CONTINUE;
            }
            if (matches(dir)) {
                return v.preVisitDirectory(dir, attrs);
            }
            else {
                return FileVisitResult.SKIP_SUBTREE;
            }
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            file = root.relativize(file);
            if (matches(file)) {
                return v.visitFile(file, attrs);
            }
            else {
                return FileVisitResult.CONTINUE;
            }
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            return v.visitFileFailed(file, exc);
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            if (matches(dir)) {
                return v.postVisitDirectory(dir, exc);
            }
            else {
                return FileVisitResult.CONTINUE;
            }
        }
        
        private boolean matches(Path path) {
            return exclude == null || !exclude.matches(path);
        }
    }
    
    public static void fileFinder(String location, final PathMatcher files, final PathMatcher exclude, final ExtendedFileVisitor<Path> v) throws IOException {
        final Path loc = Paths.get(location);
        Files.walkFileTree(loc, new TopLevelFileVisitor(loc, files, exclude, v));
        v.close();
    }
    
    public static void main(String[] args) {
        try {
            //fileFinder("/home/mike/work/pw/helyxos/newCase_summer_2", files("*"), print());
            //fileFinder(files("*"), exclude("constant/polyMesh/*", "processor*"), print());
            fileFinder(files("*"), exclude("00*", "[1-9]*"), print());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
