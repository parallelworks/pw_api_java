/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JobInfo {
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Output {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
    
    private Map<String, Output> outputs;

    public Map<String, Output> getOutputs() {
        return outputs;
    }

    public void setOutputs(Map<String, Output> outputs) {
        this.outputs = outputs;
    }
    
    public Output getOutput(String name) {
        Output o = outputs.get(name);
        if (o == null) {
            throw new IllegalArgumentException("No such output: " + name);
        }
        return o;
    }
}
