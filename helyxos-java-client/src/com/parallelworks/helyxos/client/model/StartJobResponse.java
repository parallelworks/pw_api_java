/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StartJobResponse {
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Job {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
    
    @JsonProperty("decoded_job_id")
    private String decodedJobId;
    
    private List<Job> jobs;

    public String getDecodedJobId() {
        return decodedJobId;
    }

    public void setDecodedJobId(String decodedJobId) {
        this.decodedJobId = decodedJobId;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }
}
