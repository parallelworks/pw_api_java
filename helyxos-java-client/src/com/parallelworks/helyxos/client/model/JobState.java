/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JobState {
    private String state;
    
    private String status;
    
    public JobState() {
    }
    
    public JobState(String state, String status) {
        this.state = state;
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
