/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StartJobInputs {
    
    public static class Value {
        private String src;
        private String id;
        
        public Value() {
        }
        
        public Value(String did) {
            this.src = "hda";
            this.id = did;
        }
        
        public String getSrc() {
            return src;
        }
        
        public void setSrc(String src) {
            this.src = src;
        }
        
        public String getId() {
            return id;
        }
        
        public void setId(String id) {
            this.id = id;
        }
    }
    
    public static class Case {
        private List<Value> values;
        
        public Case() {
        }
        
        public Case(String did) {
            values = Collections.singletonList(new Value(did));
        }

        public List<Value> getValues() {
            return values;
        }

        public void setValues(List<Value> values) {
            this.values = values;
        }
    }
    
    @JsonProperty("case")
    private Case _case;
    
    private String command;
    
    public StartJobInputs() {
    }
    
    public StartJobInputs(String did, String helyxosCommand) {
        this._case = new Case(did);
        this.command = helyxosCommand;
    }

    public Case getCase() {
        return _case;
    }

    public void setCase(Case _case) {
        this._case = _case;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
