/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

public class JobId {
    private String id, decodedId;
    
    public JobId(String id, String decodedId) {
        this.id = id;
        this.decodedId = decodedId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDecodedId() {
        return decodedId;
    }

    public void setDecodedId(String decodedId) {
        this.decodedId = decodedId;
    }

    @Override
    public String toString() {
        return "JobId [id=" + id + ", decodedId=" + decodedId + "]";
    }
}
