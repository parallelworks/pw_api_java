/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * {
 *  "name": "helyxos_workspace", 
 *  "tags": [], 
 *  "deleted": false, 
 *  "purged": false, 
 *  "annotation": null, 
 *  "url": "/api/histories/fab535a3ae44be6d", 
 *  "published": false, 
 *  "model_class": "History", 
 *  "id": "fab535a3ae44be6d"}
 */
public class HistoryEntry {
    private String name;
    
    private List<String> tags;
    
    private boolean deleted;
    
    private boolean purged;
    
    private String annotation;
    
    private String url;
    
    private boolean published;
    
    @JsonProperty("model_class")
    private String modelClass;
    
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean getPurged() {
        return purged;
    }

    public void setPurged(boolean purged) {
        this.purged = purged;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getModelClass() {
        return modelClass;
    }

    public void setModelClass(String modelClass) {
        this.modelClass = modelClass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "HistoryEntry [name=" + name + ", tags=" + tags + ", deleted=" + deleted
                + ", purged=" + purged + ", annotation=" + annotation + ", url=" + url
                + ", published=" + published + ", modelClass=" + modelClass + ", id=" + id
                + "]";
    }
}
