/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadDatasetResponse {
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Output {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
    
    @JsonProperty("outputs")
    private List<Output> outputs;

    public List<Output> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<Output> outputs) {
        this.outputs = outputs;
    }
}
