/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import com.parallelworks.helyxos.client.model.JobId;
import com.parallelworks.helyxos.client.model.JobState;

import static com.parallelworks.helyxos.client.FSTools.*;

public class RunCloud {
    
    private static final String PW_URL = "https://eval.parallel.works";
    private static final String DATASET_NAME = "inputs.tgz";
    private static final String WORKFLOW_NAME = "helyxos_runner_rsync_v2";
    
    private String apiKey;
    private String workspaceName;
    private String helyxosCommand;
    private PrintWriter log;
    private boolean pullResults;
    private JobId jid;
    private PWClient client;
    
    public RunCloud(String command, String apiKey, String workspace, boolean pullResults) {
        this.apiKey = apiKey;
        this.workspaceName = workspace;
        this.helyxosCommand = command;
        this.pullResults = pullResults;
    }
    
    
    public void run() throws Exception {
        removeOldPackages();
        
        packageInput();
        
        client = new PWClient(PW_URL, apiKey);
        
        String wid = client.getWid(workspaceName);
        
        System.out.print("Uploading the case");
        String did = client.uploadDataset(wid, DATASET_NAME);
        
        waitForUpload(client, did);
        
        System.out.println("Executing the case");
        
        jid = client.startJob(wid, did, WORKFLOW_NAME, helyxosCommand);
        
        writeJobIdToFile("cloud.job", jid);
        
        try {
        
            String rid = pollOutput(client, jid);

            String durl = client.getDownloadUrl(rid);
            
            System.out.println("Parallel Works Run Successful");
            
            postProcess();
        
            Writer w = new FileWriter("cloud.out");
            w.write(durl);
            w.close();
            
            System.out.println(durl);
        }
        catch (ApplicationException e) {
            System.err.println("Parallel Works Run Failed");
            System.err.println(e.getMessage());
            new File("cloud.out").delete();
            System.exit(1);
        }
    }
    
    public void shutDown() {
        if (jid != null) {
            client.cancelJob(jid);
        }
    }
    
    private void removeOldPackages() throws IOException {
        System.out.println("Compressing input files");
        fileFinder(files("*.tgz", "*.tar.gz"), deleteFile());
    }

    private void packageInput() throws IOException {
        if (helyxosCommand.contains("mesh")) {
            fileFinder(files("*"), exclude("constant/polyMesh/*", "processor*"), targz("inputs.tgz"));
        }
        else {
            fileFinder(files("*"), exclude("00*", "[1-9]*"), targz("inputs.tgz"));
        }
    }


    private void postProcess() throws IOException {
        if (helyxosCommand.contains("mesh")) {
            fileFinder(files("constant/polyMesh/*", "processor*", "00*", "[1-9]*"), deleteFile());
            System.out.println("Pulling Results on Completion:");
        }
        else {
            fileFinder(files("00*", "[1-9]*", "controlDict"), deleteFile());
            if (pullResults) {
                System.out.println("Pulling Results on Completion:");
            }
            else {
                System.out.println("Not Pulling Results on Completion:");
            }
        }
    }
    
    private void removeFiles(String pattern) throws IOException {
        PathMatcher m = FileSystems.getDefault().getPathMatcher(pattern);
        Files.walkFileTree(Paths.get("."), null, 0, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                file.toFile().delete();
                return FileVisitResult.CONTINUE;
            }
        });
    }


    private String pollOutput(PWClient client, JobId jid) throws InterruptedException, IOException, ApplicationException {
        int lastLine = 0;
        String lastStatus = "";
        
        String logFileName = System.getenv("LOG");
        if (logFileName != null) {
            log = new PrintWriter(new FileWriter(System.getenv("LOG"), true));
        }
        try {
            while (true) {
                Thread.sleep(1000);
                JobState js;
                try {
                    js = client.getJobState(jid);
                }
                catch (Exception e) {
                    //e.printStackTrace();
                    js = new JobState("starting", "");
                }

                String status = js.getStatus();
                
                if (status != null && !status.equals(lastStatus)) {
                    System.out.println(status);
                }
                lastStatus = status;
                
                String tail = client.getJobTail(jid, lastLine);
                
                if (!tail.isEmpty()) {
                    System.out.println(tail);
                    if (log != null) {
                        log.println(tail);
                        log.flush();
                    }
                    lastLine += count(tail, '\n');
                }
                
                if ("ok".equals(js.getState())) {
                    return client.getResultId(jid, "results");
                }
                else if ("deleted".equals(js.getState()) || "error".equals(js.getState())) {
                    throw new ApplicationException("Simulation had an error. Please try again");
                }
            }
        }
        finally {
            if (log != null) {
                log.close();
            }
        }
    }


    private int count(String s, char c) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (c == s.charAt(i)) {
                count++;
            }
        }
        return count;
    }


    private void writeJobIdToFile(String fileName, JobId jid) throws IOException {
        Writer wr = new FileWriter(fileName);
        wr.write(jid.getId());
        wr.close();
    }


    public void waitForUpload(PWClient client, String did) throws InterruptedException {
        while (true) {
            Thread.sleep(1000);
            String state = client.getDatasetState(did);
            if (state.equals("ok")) {
                System.out.println();
                break;
            }
            else {
                System.out.print(".");
            }
        }
    }

    public static void main(String[] args) {
        try {
            String command = args[0];
            String apiKey = args[1];
            String workspace = args[2];
            boolean pullResults = Boolean.parseBoolean(args[3]);
            
            final RunCloud rc = new RunCloud(command, apiKey, workspace, pullResults);
            
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    rc.shutDown();
                }
            });
            
            rc.run();
            
            System.exit(0);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(99);
        }
    }
}
