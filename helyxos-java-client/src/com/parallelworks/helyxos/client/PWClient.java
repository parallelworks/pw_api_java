/*
 * Created on Aug 29, 2016
 */
package com.parallelworks.helyxos.client;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.media.multipart.internal.MultiPartWriter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.parallelworks.helyxos.client.model.Dataset;
import com.parallelworks.helyxos.client.model.Histories;
import com.parallelworks.helyxos.client.model.HistoryEntry;
import com.parallelworks.helyxos.client.model.JobId;
import com.parallelworks.helyxos.client.model.JobInfo;
import com.parallelworks.helyxos.client.model.JobState;
import com.parallelworks.helyxos.client.model.StartJobInputs;
import com.parallelworks.helyxos.client.model.StartJobResponse;
import com.parallelworks.helyxos.client.model.UploadDatasetResponse;


public class PWClient {
    private String url;
    private String apiKey;
    private Client client;
    private WebTarget baseTarget;
    
    public PWClient(String url, String apiKey) {
        this.url = url;
        this.apiKey = apiKey;
                
        client = ClientBuilder.newClient();
        client.register(JacksonJsonProvider.class);
        client.register(MultiPartWriter.class);
        //client.register(new LoggingFilter());
        baseTarget = client.target(url).path("api"); 
    }
    
    public String getWid(String name) { 
        Histories histories = getHistories();
        
        for (HistoryEntry e : histories) {
            if (name.equals(e.getName())) {
                return e.getId();
            }
        }
        
        return null;
    }
    
    public String uploadDataset(String wid, String fileName) throws IOException {
        File f = new File(fileName);
        FileDataBodyPart filePart = new FileDataBodyPart("files_0|file_data", f);
        FormDataMultiPart formData = new FormDataMultiPart();
        formData.field("key", apiKey);
        formData.field("tool_id", "upload1");
        formData.field("workspace_id", wid);
        formData.bodyPart(filePart);
      
        Response response = baseTarget.path("tools").request().post(Entity.entity(formData, formData.getMediaType()));  
        formData.close();
        return response.readEntity(UploadDatasetResponse.class).getOutputs().get(0).getId();
    }

    public Histories getHistories() {
        return baseTarget.path("histories").queryParam("key", apiKey).request().get(Histories.class);
    }
    
    public Dataset getDataset(String did) {
        return baseTarget.path("datasets").path(did).queryParam("key", apiKey).request().get(Dataset.class);
    }
    
    public String getDatasetState(String did) {
        return getDataset(did).getState();
    }
    
    public String getWorkflowName(String workflow) {
        return client.target(url).path("workflow_name").path(workflow).queryParam("key", apiKey).request().get(String.class);
    }
    
    public JobId startJob(String wid, String did, String workflow, String helyxosCommand) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        StartJobInputs inputs = new StartJobInputs(did, helyxosCommand);
        
        FormDataMultiPart formData = new FormDataMultiPart();
        formData.field("key", apiKey);
        formData.field("tool_id", getWorkflowName(workflow));
        formData.field("workspace_id", wid);
        formData.close();
        try {
            formData.field("inputs", mapper.writeValueAsString(inputs));
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        
        Response resp = baseTarget.path("tools").request().post(Entity.entity(formData, formData.getMediaType()));
        StartJobResponse sjr = resp.readEntity(StartJobResponse.class);
        
        return new JobId(sjr.getJobs().get(0).getId(), sjr.getDecodedJobId());
    }
    
    public JobState getJobState(JobId jid) {
        return getJobState(jid.getId());
    }
    
    public JobState getJobState(String jid) {
        // galaxy is sending the JSON back as text/html
        String resp = baseTarget.path("jobs").path(jid).path("state").queryParam("key", apiKey).request(MediaType.APPLICATION_JSON_TYPE).get(String.class);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(resp, JobState.class);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    
    public String getJobTail(JobId jid, int lastLine) {
        return getJobTail(jid.getDecodedId(), lastLine);
    }
    
    public String getJobTail(String djid, int lastLine) {
        return client.target(url).path("tail_change").path(djid)
                .queryParam("file", "_stdout_1.txt").queryParam("line", lastLine).request().get(String.class);
    }
    
    public String getResultId(JobId jid, String name) {
        return getResultId(jid.getId(), name);
    }
    
    public String getResultId(String jid, String name) {
        return baseTarget.path("jobs").path(jid).queryParam("key", apiKey).request().get(JobInfo.class).getOutput(name).getId();
    }
    
    public String getDownloadUrl(String did) {
        return url + getDataset(did).getDownloadUrl();
    }
    
    public String uploadFileToJob(String jid, String fileName) throws IOException {
        File f = new File(fileName);
        FileDataBodyPart filePart = new FileDataBodyPart("file", f);
        FormDataMultiPart formData = new FormDataMultiPart();
        formData.bodyPart(filePart);
      
        Response response = baseTarget.path("jobs").path(jid).path("upload").queryParam("key", apiKey).request().post(Entity.entity(formData, formData.getMediaType()));  
        formData.close();
        return response.readEntity(String.class);
    }
    
    public String cancelJob(JobId jid) {
        return cancelJob(jid.getId());
    }
    
    public String cancelJob(String jid) {
        return baseTarget.path("jobs").path(jid).path("cancel").queryParam("key", apiKey).request().get(String.class);
    }
    
    public static void main(String[] args) {
        try {
            PWClient cl = new PWClient("https://eval.parallel.works", "");
            String wid = cl.getWid("helyxos_workspace");
            System.out.println(wid);
            String did = cl.uploadDataset(wid, "test.txt");
            System.out.println(did);
            
            System.out.println(cl.getDataset(did));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
